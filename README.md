# Install Suricata with PF_RING


Suricata is an open source-based intrusion detection system and intrusion prevention system.

PF_RING is a new type of network socket that dramatically improves the packet capture speed.

The shell script will resolve the dependencies and install suricata and pf_ring.


Use:

`sudo su -`

`chmod +x install_suricata.sh`

`./install_suricata.sh`


