# add epel repo
amazon-linux-extras install epel -y

# install rust
cd ~/
curl https://sh.rustup.rs -sSf | sh


# instal cargo
yum install rustc cargo

sleep 5s

# install libyaml
cd ~/
wget http://pyyaml.org/download/libyaml/yaml-0.2.2.tar.gz
tar -xvf yaml-0.2.2.tar.gz
cd yaml-0.2.2/
./configure
make
make install

# install jansson
cd ~/
wget http://www.digip.org/jansson/releases/jansson-2.12.tar.gz
tar -xvf jansson-2.12.tar.gz
cd jansson-2.12
./configure
make
make check
make install

# install libpcap
cd ~/
wget http://www.tcpdump.org/release/libpcap-1.5.3.tar.gz
tar -xf libpcap-1.5.3.tar.gz
cd libpcap-1.5.3
./configure
make && make install

sleep 5s

# install llvm7.0
yum install llvm7.0 -y

sleep 5s

# install llvm
cd ~/
curl http://rpmfind.net/linux/fedora/linux/releases/28/Everything/x86_64/os/Packages/l/llvm34-libs-3.4.2-10.fc26.x86_64.rpm
rpm -ivh llvm34-libs-3.4.2-10.fc26.x86_64.rpm

# install nDPI
cd ~/
git clone https://github.com/ntop/nDPI.git
cd nDPI
./autogen.sh
./configure
make
sudo make install


# install libpcap
cd ~/
wget http://www.tcpdump.org/release/libpcap-1.9.0.tar.gz
tar -xvf libpcap-1.9.0.tar.gz
cd libpcap-1.9.0
./configure
make
make install
sudo cp libpcap* /usr/local/lib/; sudo cp pcap.h /usr/local/include/


# compile and install pf_ring

cd ~/
git clone https://github.com/ntop/PF_RING.git
cd PF_RING/
make
cd kernel; sudo make install
cd ../userland/lib; sudo make install
sleep 5s
cd ../userland/libpcap; ./configure; make

sleep 5s
# verify pf_ring

modinfo pf_ring
cat /proc/net/pf_ring/info


# clone suricata
cd ~/
git clone https://github.com/OISF/suricata
cd suricata
git clone https://github.com/OISF/libhtp

chmod +x autogen.sh

./autogen.sh

sleep 10s
# enable suricata with pf_ring suricata

LIBS="-lrt -lnuma"
./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
--enable-pfring --with-libpfring-includes=/usr/local/include \
--with-libpfring-libraries=/usr/local/lib


make
sudo make install
sudo ldconfig

sudo make install-conf
sudo make install-rules

sleep 5s
# run suricata
#sudo suricata --pfring-int=eth0 --pfring-cluster-id=99 \
#-c /etc/suricata/suricata.yaml -D


# clean up rust cargo if necessary
# rm -rf ~/.cargo/registry/index/*


# install suricata as a service
cd ~/install-suricata-with-pf_ring/
sudo cp suricata_service.sh /usr/bin/suricata_service.sh
sudo chmod 744 /usr/bin/suricata_service.sh
sudo cp suricata.service /etc/systemd/system/suricata.service

sudo systemctl enable suricata.service
sudo systemctl start suricata.service
sudo systemctl status suricata.service

suricata --build-info | grep PF_RING


